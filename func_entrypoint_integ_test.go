// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package publish2fs

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegEntryPoint(t *testing.T) {
	testCases := []struct {
		name            string
		path            string
		messageType     string
		origin          string
		assetType       string
		contentType     string
		missPublishTime bool
		wantMsgContains []string
		checkDocContent bool
		checkExpireAt   bool
	}{
		{
			name:            "missing_time",
			messageType:     "asset_feed",
			assetType:       "cloudresourcemanager.googleapis.com/Project",
			contentType:     "RESOURCE",
			missPublishTime: true,
			path:            "testdata/asset_01.json",
			wantMsgContains: []string{
				"noretry",
				"triggering PubSub message attribute 'timestamp' is missing or invalid and should not",
			},
		},
		{
			name:        "wrong message-type",
			messageType: "blabla",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			contentType: "RESOURCE",
			path:        "testdata/asset_01.json",
			wantMsgContains: []string{
				"noretry",
				"want triggering PubSub message attribute 'messageType' to be 'asset_feed' and got",
			},
		},
		{
			name:        "missing origin",
			messageType: "asset_feed",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			contentType: "RESOURCE",
			path:        "testdata/asset_01.json",
			wantMsgContains: []string{
				"noretry",
				"triggering PubSub message attribute 'origin' is zero value and should not",
			},
		},
		{
			name:        "missing assetType",
			messageType: "asset_feed",
			origin:      "real-time",
			assetType:   "",
			contentType: "RESOURCE",
			path:        "testdata/asset_01.json",
			wantMsgContains: []string{
				"noretry",
				"triggering PubSub message attribute 'assetType' is zero value and should not",
			},
		},
		{
			name:        "missing contentType",
			messageType: "asset_feed",
			origin:      "real-time",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			contentType: "",
			path:        "testdata/asset_01.json",
			wantMsgContains: []string{
				"noretry",
				"triggering PubSub message attribute 'contentType' is zero value and should not",
			},
		},
		{
			name:        "still_works_when_missing_stepstack_in_json",
			messageType: "asset_feed",
			origin:      "real-time",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			contentType: "RESOURCE",
			path:        "testdata/asset_03.json",
			wantMsgContains: []string{
				"finish set document",
			},
		},
		{
			name:        "set_project_doc",
			messageType: "asset_feed",
			origin:      "real-time",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			contentType: "RESOURCE",
			path:        "testdata/asset_01.json",
			wantMsgContains: []string{
				"finish set document",
				"assets/\\\\\\\\cloudresourcemanager.googleapis.com\\\\projects\\\\325783050894",
			},
		},
		{
			name:        "set_project_iam_doc",
			messageType: "asset_feed",
			origin:      "real-time",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			contentType: "IAM_POLICY",
			path:        "testdata/asset_04.json",
			wantMsgContains: []string{
				"finish set document",
				"assets/\\\\\\\\cloudresourcemanager.googleapis.com\\\\projects\\\\83314339016_IAM_POLICY",
			},
			checkDocContent: true,
		},
		{
			name:        "set_gke_cluster_rce_doc",
			messageType: "asset_feed",
			origin:      "real-time",
			assetType:   "container.googleapis.com/Cluster",
			contentType: "RESOURCE",
			path:        "testdata/asset_05.json",
			wantMsgContains: []string{
				"finish set document",
				"assets/\\\\\\\\container.googleapis.com\\\\projects\\\\qwertyu-ram-noncompliant\\\\zones\\\\us-central1-c\\\\clusters\\\\cluster-1",
			},
			checkDocContent: true,
			checkExpireAt:   true,
		},
		{
			name:        "delete_project_doc",
			messageType: "asset_feed",
			origin:      "real-time",
			assetType:   "cloudresourcemanager.googleapis.com/Project",
			contentType: "RESOURCE",
			path:        "testdata/asset_02.json",
			wantMsgContains: []string{
				"finish delete document",
				"assets/\\\\\\\\cloudresourcemanager.googleapis.com\\\\projects\\\\325783050894",
			},
		},
	}
	ctx := context.Background()

	now := time.Now()

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			var attr map[string]string
			var pubsubMsg pubsub.Message
			attr = make(map[string]string)
			attr["assetType"] = tc.assetType
			attr["contentType"] = tc.contentType
			attr["messageType"] = tc.messageType
			attr["microserviceName"] = "convertfeed"
			attr["origin"] = tc.origin
			if !tc.missPublishTime {
				attr["timestamp"] = now.Format(time.RFC3339)
			}

			pubsubMsg.Attributes = attr
			pubsubMsg.ID = "c56c9245-0af7-44c7-8a55-954583940e09"

			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			pubsubMsg.Data = b

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			_ = EntryPoint(ctx, pubsubMsg)
			msgString := buffer.String()
			// t.Logf("%v", msgString)
			for _, wantMsg := range tc.wantMsgContains {
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s", tc.wantMsgContains)
				}
			}
			logEntriesString := strings.Split(msgString, "\n")
			for _, logEntryString := range logEntriesString {
				var logEntry glo.Entry
				err = json.Unmarshal([]byte(logEntryString), &logEntry)
				if !(err != nil) {
					if logEntry.MicroserviceName == "" {
						t.Errorf("logEntry.MicroserviceName is null should not %s", logEntry.Message)
					}
					if logEntry.Environment == "" {
						t.Errorf("logEntry.Environment is null should not %s", logEntry.Message)
					}
					if strings.HasPrefix(strings.ToLower(logEntry.Message), "finish") {
						if len(logEntry.StepStack) == 0 {
							t.Errorf("logEntry.StepStack len is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencyE2ESeconds == 0 {
							t.Errorf("logEntry.LatencyE2ESeconds is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencySeconds == 0 {
							t.Errorf("logEntry.LatencySeconds is zero and should not %s", logEntry.Message)
						}
					}
				}
			}
			if tc.checkDocContent {
				var originalFeedMessage cai.FeedMessage
				err = json.Unmarshal(b, &originalFeedMessage)
				if err != nil {
					t.Errorf("json.Unmarshal %v", err)
				}

				documentID := strings.ReplaceAll(originalFeedMessage.Asset.Name, "/", "\\")
				if originalFeedMessage.ContentType != "RESOURCE" && originalFeedMessage.ContentType != "" {
					documentID = fmt.Sprintf("%s_%s", documentID, originalFeedMessage.ContentType)
				}
				documentPath := global.serviceEnv.AssetCollectionID + "/" + documentID
				var ev glo.EntryValues
				ev.CommonEntryValues = global.CommonEv

				documentSnap, getDocOk := gfs.GetDocWithRetry(ctx, global.firestoreClient, documentPath, 5, false, ev)
				if getDocOk {
					var fsFeedMessage cai.FeedMessageFS
					err = documentSnap.DataTo(&fsFeedMessage)
					if err != nil {
						t.Errorf("documentSnap.DataTo %v", err)
					}
					feedJSON, err := json.Marshal(&fsFeedMessage)
					if err != nil {
						t.Errorf("json.Marshal(&fsFeedMessage %v", err)
					}
					var retrieveFeedMessage cai.FeedMessage
					err = json.Unmarshal(feedJSON, &retrieveFeedMessage)
					if err != nil {
						t.Errorf("json.Unmarshal(feedJSON %v", err)
					}
					o, _ := json.MarshalIndent(&originalFeedMessage.Asset, "", "    ")
					r, _ := json.MarshalIndent(&retrieveFeedMessage.Asset, "", "    ")
					// t.Logf("%s", r)
					if string(o) != string(r) {
						t.Errorf("want retrieve feed message asset from firestore equal the original, and is not")
					}
					if tc.checkExpireAt {
						if fsFeedMessage.ExpireAt.IsZero() {
							t.Errorf("want an expire timestamp and did not found it")
						}
					}
				} else {
					t.Errorf("try to retrieve the firestore document and did not found it %s", documentPath)
				}
			}
		})
	}
}
